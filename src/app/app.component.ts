import {Component} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {Cycle, Version, Date} from "../model/model";
import {DoubleSelectComponent} from "../library/form-components/double-select/double-select.component";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, DoubleSelectComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'geneRick';

  cycleList: Cycle[] = [];
  versionList: Version[]= [];
  dateList: Date[];

  private selectedCycle: FormControl<any>;
  private selectedVersion: FormControl<any>;
  private selectedDate: FormControl<any>;

  constructor() {
    console.log('constructor')
    this.getCycles();
  }

  private getCycles() {
    //call service
    this.cycleList   = [
      { id: "1", label: 'Cycle 1' },
      { id: "2", label: 'Cycle 2' }
    ];

    this.getVersions();
  }

  private getVersions() {
    //call service
    this.versionList = [
      { id: "1", label: 'Version 1' },
      { id: "2", label: 'Version 2' }
    ];

    this.getDates();
  }

  private getDates() {
    //call service
    this.dateList = [
      { date: "19/01/2024" },
      { date: "20/01/2024" },
      { date: "21/01/2024" },
    ];
  }

  handleFormChange(formData: any) {
    this.selectedCycle = formData.selectedCycle;
    this.selectedVersion = formData.selectedVersion;
    this.selectedDate = formData.selectedDate;
    console.log(this.selectedCycle)
    console.log(this.selectedVersion)
    console.log(this.selectedDate)
  }
}
