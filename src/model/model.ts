export interface Cycle {
  id: string;
  label: string;
}

export interface Version {
  id: string;
  label: string;
}

export interface Date {
  date: string;
}
