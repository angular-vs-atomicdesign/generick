import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatOption, MatSelect} from "@angular/material/select";
import {NgForOf, NgIf} from "@angular/common";
import {MatCard, MatCardContent} from "@angular/material/card";
import {FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {Cycle, Version, Date} from "../../../model/model";

@Component({
  selector: 'double-select',
  standalone: true,
  imports: [
    MatFormField,
    MatSelect,
    MatOption,
    MatLabel,
    NgForOf,
    MatCard,
    MatCardContent,
    NgIf,
    FormsModule,
    ReactiveFormsModule
  ],
  templateUrl: './double-select.component.html',
  styleUrl: './double-select.component.css'
})
export class DoubleSelectComponent implements OnInit, OnChanges {

  @Input() cycles: Cycle[];
  @Input() versions: Version[];
  @Input() dates: Date[];

  @Output()
  onFormChangeEvent: EventEmitter<any> = new EventEmitter<any>();

  cycleLabel: string = 'Cycle';
  versionLabel: string = 'Version';
  dateLabel: string = 'Date';

  form: FormGroup;

  constructor() {
    this.form = new FormGroup({
      selectedCycle: new FormControl(null, Validators.required),
      selectedVersion: new FormControl(null, Validators.required),
      selectedDate: new FormControl(null, Validators.required)
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['cycles'] && this.cycles && this.cycles.length > 0) {
      // @ts-ignore
      this.form.get('selectedCycle').setValue(this.cycles[0]);
    }
    if (changes['versions'] && this.versions && this.versions.length > 0) {
      // @ts-ignore
      this.form.get('selectedVersion').setValue(this.versions[0]);
    }
    if (changes['dates'] && this.dates && this.dates.length > 0) {
      // @ts-ignore
      this.form.get('selectedDate').setValue(this.dates[0]);
    }
  }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(_ => this.onFormChange());
  }

  onFormChange() {
    this.onFormChangeEvent.emit(this.form.value);
  }
}
