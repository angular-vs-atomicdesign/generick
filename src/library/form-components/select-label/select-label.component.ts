import {Component, Input} from '@angular/core';
import {FormControl, ReactiveFormsModule} from "@angular/forms";
import {MatFormField} from "@angular/material/form-field";
import {MatOption, MatSelect} from "@angular/material/select";
import {DisplayValuePipe} from "../../../pipes/display-value.pipe";
import {NgForOf} from "@angular/common";

export interface BaseEntity {
  id?: number;
  label?: string;
  code?: string;
}

@Component({
  selector: 'select-label',
  standalone: true,
  imports: [
    MatFormField,
    MatSelect,
    ReactiveFormsModule,
    MatOption,
    DisplayValuePipe,
    NgForOf
  ],
  templateUrl: './select-label.component.html',
  styleUrl: './select-label.component.css'
})
export class SelectLabelComponent<T extends BaseEntity> {

  @Input()
  options: T[] = [];
  @Input()
  control: FormControl;

  label = 'chien';

  @Input()
  display: (value: T) => string;

}
