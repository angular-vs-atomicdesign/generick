import {Component, OnInit} from '@angular/core';
import {Cycle} from "../../../model/model";
import {FormBuilder, FormGroup} from "@angular/forms";
import {SelectLabelComponent} from "../select-label/select-label.component";
import {FormControlFromGroupPipe} from "../../../pipes/form-control-from-group.pipe";

@Component({
  selector: 'multi-select',
  standalone: true,
  imports: [
    SelectLabelComponent,
    FormControlFromGroupPipe
  ],
  templateUrl: './multi-select.component.html',
  styleUrl: './multi-select.component.css'
})
export class MultiSelectComponent implements OnInit {
  display: (value: Cycle) => string = (value) => {
    return value?.label;
  };

  readonly cycleFormKey = 'cycle';
  readonly versionFormKey = 'version';
  readonly dateFormKey = 'date';

  form: FormGroup = this.formBuilder.group({
    [this.cycleFormKey]: null,
    [this.versionFormKey]: null,
    [this.dateFormKey]: null
  });

  cycleList: Cycle[] = [{
    label: 'chien',
    id: '1'
  }, {
    label: 'chat',
    id: '2'
  }, {
    label: 'lapin',
    id: '3'
  },
  ];

  constructor(private readonly formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(_ => this.onFormChange())
  }


  private onFormChange() {
    console.log(this.form.value);
  }
}
