import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  standalone: true,
  name: 'displayValue'
})
export class DisplayValuePipe implements PipeTransform {

  transform<T>(value: T, displayWith: (val : T) => string): unknown {
    return displayWith(value);
  }

}
