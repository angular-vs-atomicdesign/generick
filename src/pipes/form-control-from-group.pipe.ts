import { Pipe, PipeTransform } from '@angular/core';
import {FormGroup} from "@angular/forms";

@Pipe({
  standalone: true,
  name: 'formControlFromGroup'
})
export class FormControlFromGroupPipe implements PipeTransform {

  transform(value: FormGroup, formKey: string): unknown {
    return value.get(formKey);
  }

}
